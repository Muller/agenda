import java.io.*;

public class CadastroPessoa{

  public static void main(String[] args) throws IOException{
    //burocracia para leitura de teclado
    InputStream entradaSistema = System.in;
    InputStreamReader leitor = new InputStreamReader(entradaSistema);
    BufferedReader leitorEntrada = new BufferedReader(leitor);
    String entradaTeclado;

    //instanciando objetos do sistema
    ControlePessoa umControle = new ControlePessoa();
    Pessoa umaPessoa = new Pessoa();

    //interagindo com usuário
    System.out.println("Digite o nome da Pessoa:");
    entradaTeclado = leitorEntrada.readLine();
    String umNome = entradaTeclado;
    umaPessoa.setNome(umNome);

    System.out.println("Digite o telefone da Pessoa:");
    entradaTeclado = leitorEntrada.readLine();
    String umTelefone = entradaTeclado;
    umaPessoa.setTelefone(umTelefone);

    //adicionando uma pessoa na lista de pessoas do sistema
    String mensagem = umControle.adicionar(umaPessoa);

    //conferindo saída
    System.out.println("=================================");
    System.out.println(mensagem);
    System.out.println("=)");

  }

}
